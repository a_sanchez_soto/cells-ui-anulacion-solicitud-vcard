import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-anulacion-solicitud-vcard-styles.js';
import '@bbva-web-components/bbva-form-field'
import '@bbva-web-components/bbva-button-default';
import '@cells-components/cells-select';
import '@vcard-components/cells-theme-vcard';
import '@vcard-components/cells-util-behavior-vcard';
import '@cells-components/cells-icon';
import '@vcard-components/cells-ui-modal-vcard';

/**
This component ...

Example:

```html
<cells-ui-anulacion-solicitud-vcard></cells-ui-anulacion-solicitud-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiAnulacionSolicitudVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-anulacion-solicitud-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      name: { type: String, },
      msjAlerta: {type: String},
      detalleAnulacion: {type: String},
      contar:{type: Number},
      motivos: {
        type: Array
      },
      numSolicitud: {
        type: String
      },
      motivoSelected:{
        type: Object
      }
    };
  }

  
  // Initialize properties
  constructor() {
    super();
    this.motivos=[];
    this.numSolicitud='';
    this.detalleAnulacion='';
    this.msjAlerta = '';
    this.contar = 0;
    this.motivoSelected = {};
    this.updateComplete.then(() => {
      this.inputSelectLoadData(this.events.loadDataCellsSelect, this);
    });
  }

  anularSolictud(event) {
    var validacion = this.validarDatosIngreso();
    if(validacion==0){
      console.log('anularSolictud', event);
      let detalle = this.shadowRoot.querySelector('textarea');
      let anulacion = {
        codigo: this.numSolicitud,
        motivo: this.motivoSelected.value.record._id ? this.motivoSelected.value.record._id : '',
        descripcionMotivo:  detalle.value ? detalle.value : ''
      };
      this.dispatch(this.events.anularSolicitudEvent, anulacion);
      this.limpiarDatos();
    }
 
  }

  cancelarAnulacion(event) {
    this.limpiarDatos();
    console.log('cancelarAnulacion', event);
    this.dispatch('cancelar-anulacion-event', {});

    //this.dispath(this.events.closeWindowAnulacion,{})

  }
  
  limpiarDatos(){

    let value = this.getById('txtDetalle');
    value.value ="";
    
    let valueMotivo = this.getById('cmbMotivo');
    valueMotivo.value=0;
    this.contar=0;
    
  }


  validarDatosIngreso(){

    var correcto= 0;
    //var anulacionaf     = new Object();
    //let motivo = this.shadowRoot.querySelector('cells-select');
    let txtAdetalle = this.shadowRoot.querySelector('textarea');
    //var motivo  = this.getSelectedIndex('cmbMotivo', this.motivos);
    var motivo = this.getInputValue('cmbMotivo');
    var detalle = this.getInputValue('txtDetalle');
    var numeroPalabras = detalle.length;

    //alert("alerta Motivo="+ motivo + "Detalle"+ detalle + " numero= " + numeroPalabras );
    this.msjAlerta='';
    if(motivo == undefined || motivo == ""){
      //alert("alerta 1");
      this.msjAlerta = ' Seleccione un motivo.';
      correcto=1;
    }else
    if(detalle == undefined || detalle == "" || detalle == " "){
      //alert("alerta 2");
      this.msjAlerta = ' Escriba un detalle';
      correcto=1;
    }else if(numeroPalabras >= 100){
      //alert("alerta 3");
      this.msjAlerta = ' Máximo de letras permitidas 100';
      correcto=1;
    }
    
    return correcto;
  }

  htmlMensajeAlerta(){
    if(this.msjAlerta !== ''){
      return html`<div class='error'>
        <cells-icon icon='coronita:info'></cells-icon>
        <p>${this.msjAlerta}</p>
      </div>`;
    }else{
      return '';
    }
  }

  contarCaracteres(){
    let value = this.getById('txtDetalle');
    this.contar = value? value.textLength: this.contar;
  }

  onChangeMotivo(event) {
    this.motivoSelected = event.detail;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-anulacion-solicitud-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class='contenedorAnulacion'>
        <div class='fila-anular'>
          <span class='titulo'>Número de solicitud: </span><span class='subTitulo'>${this.numSolicitud}<span>
        </div>
        <div class='fila-anular'>
          <cells-select 
            id="cmbMotivo"
            data-label="nombre" 
            data-value="_id" 
            data-is-key-value="true"
            label = "Motivo anulación" 
            data-path='${this.services.endPoints.valores.motivos}' 
            @selected-option-changed="${(e) => { this.selectedOptionChanged(e, (event) => { this.onChangeMotivo(event);});}}"
            ></cells-select>
        </div>
        <div class='fila-anular'>
          <div class = "label"> Detalle de Anulación  ${this.contar} (max 100 palabras)    </div>
          <textarea maxlength="100" id='txtDetalle' placeholder="Detalle de la anulación" value="${this.detalleAnulacion}" @keyup='${()=> { this.contarCaracteres() }}'> </textarea>

        </div>
        ${this.htmlMensajeAlerta()}
        <div class = "content-button fila-anular">
          <bbva-button-default @click = "${ ()=>{this.anularSolictud()}}" text = "Anular"></bbva-button-default>
          <bbva-button-default @click = "${ ()=>{this.cancelarAnulacion()}}" text = "Limpiar"></bbva-button-default>
        </div>
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiAnulacionSolicitudVcard.is, CellsUiAnulacionSolicitudVcard);
