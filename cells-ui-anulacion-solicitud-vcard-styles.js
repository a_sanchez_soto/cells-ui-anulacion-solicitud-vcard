import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-anulacion-solicitud-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

cells-select {
  margin-top: 5px; }

textarea {
  margin-top: 5px;
  border: 1px solid #BEBEBE;
  border-bottom: 1px solid #333333;
  height: 250px;
  background-color: #f4f4f4;
  width: 100%;
  font-size: 0.9rem;
  padding: 15px;
  font-family: var(--cells-fontDefault, sans-serif); }

.content-button {
  width: 100%;
  text-align: center; }

.titulo {
  color: #004481;
  font-weight: bold; }

.contenedorAnulacion {
  margin: 0 0 3% 0; }

.fila-anular {
  margin-top: 10px; }
`;